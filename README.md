# Visualization of html graphs 
The goal of this project is to provide a simple and attractive visualization tool for networks for Python.

This work relies on the [news-graph project](https://github.com/BrambleXu/news-graph) distributed under the MIT license, which uses javascript internally.

If you want to generate a graph from scratch you just need to have a list of nodes of the right format, i.e. the nodes, their labels and their groups.

You also have the possibility to represent graphs from networkx in html.

<div style="display: flex; justify-content: center;">
 <img src="Pictures/graph.jpg" height="250">
</div>

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) 

# SET UP
pip install ANIBBLE-X==0.7.0

## IMPORTATION
To use this tool, you must have access to:
* python 3 
* opencv
* networkx
* pandas

Be careful to activate javascript to visualize the graphs on your browser. 

# Some notes:
You have various options that can be changed. Especially concerning the design of the graph. A tip, there are a lot of parameters that can be provided as a dictionary. 

Possibility of:
* Put groups to change the colors of the nodes.

* Manage sizes and colors but in a global way. 

### Tutorials:
Look at the notebook if needed.
To have an idea of possible realisation with this package, have a look at my second project indexing to html. 

### Authors:

+ Mélanie Sawaryn: melanie.sawaryn @ insa-lyon.fr

### Tutor:

+ Sergio Peignier: sergio.peignier @ insa-lyon.fr

